//******************************************************************************
//        Software License Agreement
//
// ©2016 Microchip Technology Inc. and its subsidiaries. You may use this
// software and any derivatives exclusively with Microchip products.
//
// THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
// EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
// WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR
// PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY
// OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
// IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
// INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
// WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
// BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
// FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
// ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
// THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
// MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.
//******************************************************************************

#define  STX   0x55

#define  READ_VERSION   0
#define  READ_FLASH     1
#define  WRITE_FLASH    2
#define  ERASE_FLASH    3
#define  READ_EE_DATA   4
#define  WRITE_EE_DATA  5
#define  READ_CONFIG    6
#define  WRITE_CONFIG   7
#define  CALC_CHECKSUM  8
#define  RESET_DEVICE   9
#define  GET_REGISTRY   10
#define  START_DL   	11

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "bootload.h"
#include "mcc.h"
#include "timers.h"
#include "eusart2.h"

uint8_t RdData ();
void    WrData (uint8_t  data);
extern  volatile frame_t  frame;
extern  void StartWrite(void);
void 	 Check_Device_Reset (void);

       uint16_t char_timer;      // 
extern uint16_t wait_timer;    
//-----------------------------------------
uint8_t CheckTimer ()
{
	// Which interrupt has occurred?
	if (INTCONbits.TMR0IF)
    {
    	// TMR0 has overflowed  - clear the interrupt flag
      	INTCONbits.TMR0IF = 0;
		// RESTART the timer for the next interrupt
		TMR0H = 0xFF;
		TMR0L = (char) (TIMERS_5_MS);
		// Update the software timers
		++wait_timer;
		++char_timer;
	}
	return 0;			
}

char cmd[256];
char ix;
uint8_t  msg_length, index;
char rcv_checksum;
/***********************************************************
    Subroutine:         ControlWrite_BB_Version()

    Description:
		If the Bootblock version in EEprom does not match the Flash ROM version
		the the version must be written to EEprom locations 0x3FC-0x3FD  
                    
    Inputs: 
            
    Outputs:
            
*************************************************************/
void ControlWrite_BB_Version ()
{
char i;
char eever[3];
// Read the version number out of EEprom
EEADR  = 0xFC;
EEADRH = 0x03;
EECON1 = 0;
EECON1bits.RD = 1;
eever[0] = EEDATA;
++EEADR;
EECON1bits.RD = 1;
eever[1] = EEDATA;
if ((eever[0] != SET_BB_VERSION_LO) || (eever[1] != SET_BB_VERSION_HI))
 {
	// Save the version number to eeprom
	eever[0] = SET_BB_VERSION_LO;
	eever[1] = SET_BB_VERSION_HI;
    EEADR  = 0xFB;
	EECON1 = 0x04;  //   b'00000100';     // Setup for writing EEData
    for (i = 0; i < 2; i++)
    {
        while (EECON1bits.WR == 1);  // wait until previous write complete
        ++EEADR;
        EEDATA = eever[i];
	    EECON2 = 0x55;
	    EECON2 = 0xAA;
	    EECON1bits.WR = 1;       // Start the write
	    NOP();
	    NOP();
    }
    while (EECON1bits.WR == 1);  // wait until previous write complete
 }
}

/*****************************************************************************
//Autobaud:
//
// ___     ___     ___     ___     ___     __________
//    \_S_/ 1 \_0_/ 1 \_0_/ 1 \_0_/ 1 \_0_/ Stop
//       |                                |
//       |-------------- p ---------------|
//
// EUSART autobaud works by timing 4 rising edges (0x55).  It then uses the
// timed value as the baudrate generator value.

Receives:
 respond = 1 = transmit response to GET VERSION Command
		 = 0 = remain silent and wait for command from server.

 wait	= 1 = wait 10 seconds for rs-232 input.  If no input, jump to the application
		= 0 = no need to wait.  Simply stay in the bootblock forever
*****************************************************************************/
void Run_Bootloader(char respond, char wait)
{
    uint8_t  i, timeout;

	ix = 0;
	// Write the Bootblock version number to EEprom
	ControlWrite_BB_Version ();

	// Check to see if we need to respond to a GET Version Command
	if (respond)
	{
		// Yes - we do need to respond --- set up for a response to a Get Version command
		memset (frame.buffer, 0, BB_LENGTH+2);

		// It's a Start Download Command, not a Get Version Command
		frame.command = START_DL;
		frame.data[1] = 'M';

        msg_length = ProcessBootBuffer ();

		// Send the data buffer back.
        EUSART2_Write(STX);
        index = 0;
        while (index < msg_length)
        {
            EUSART2_Write (frame.buffer [index++]);
        }
    }
//-----------------------------------------------

    while (1)
    {
        while (TXSTA2bits.TRMT == 0); // wait for last byte to shift out before
                                 // starting autobaud.
        Check_Device_Reset ();  // Response has been sent.  Check to see if a reset was requested

// *****************************************************************************
// Hardware AutoBaud
// *****************************************************************************
        BAUDCON2bits.ABDEN = 1;    // start auto baud
        while (BAUDCON2bits.ABDEN == 1)
        {
            if (BAUDCON2bits.ABDOVF == 1)
            {
                BAUDCON2bits.ABDEN = 0;    // abort auto baud
                BAUDCON2bits.ABDOVF = 0;    // start auto baud
                BAUDCON2bits.ABDEN = 1;    // restart auto baud
            }

	        // Service the Watchdog Timer
	        CLRWDT();

			// bh - if app is present, and no msg received to stay in the bootblock, still, wait 10 seconds before exiting
			if (wait)
			{
				// Check to see if 10 seconds has passed
				CheckTimer ();
				if (wait_timer >= TIMERS_WAIT_PERIOD)
				{
					// Time has expired.  We can exit the bootblock.
					return;
				}
			}	
        }
        index = EUSART2_Read();  // required to clear RCIF

// *****************************************************************************

// *****************************************************************************
// Read and parse the data.
		timeout = 0;
        index = 0;       			// Point to the buffer
        msg_length = BB_LENGTH;  // message has 9 bytes of overhead (Opcode + Length + Address)
        uint8_t  ch;

		char_timer = 0;
		rcv_checksum = 0;
        while (index < msg_length)
        {
            if(EUSART2_is_rx_ready())
            {
	            ch = EUSART2_Read();          // Get the data
				// Update the checksum, but don't update the 9th byte of the header
				if (index != BB_CS_INDEX)
				{													   
					// update the received checksum
					rcv_checksum += ch;
				}
	            frame.buffer [index ++] = ch;
				char_timer = 0;
	            if (index == 4)
	            {
	                if ((frame.command == WRITE_FLASH)
	                 || (frame.command == WRITE_EE_DATA)
	                 || (frame.command == START_DL)
	                 || (frame.command == WRITE_CONFIG))
	                {
	                    msg_length += frame.data_length;
	                }
	            }
			}
			else
			{
				// No data received yet.  Watch for timeout
				if (index > 0)
				{
					CheckTimer ();
					// 4 = 20ms delay
					if (char_timer >= 4)
					{
						frame.data[0] = ERROR_INVALID_COMMAND;
						frame.data_length = 1;
						msg_length = 10;
						index = 11;
						timeout = 1;
					}
				}
			}
        }

		if ((frame.command <= START_DL)
		 && (!timeout))
//		 && (rcv_checksum == frame.checksum))
		{
			// If a valid bootblock command has been received, wait should be turned off
			cmd[ix++] = frame.command;	// bh - debug
			wait = 0;
	        msg_length = ProcessBootBuffer ();
		}
		else
		{
			frame.data[0] = ERROR_INVALID_COMMAND;
			frame.data_length = 1;
			msg_length = 10;
		}
			// *****************************************************************************
			// Send the data buffer back.
			// *****************************************************************************
send_data:
        EUSART2_Write(STX);
        index = 0;
        while (index < msg_length)
        {
            EUSART2_Write (frame.buffer [index++]);
        }
    }
}


