//******************************************************************************
//        Software License Agreement
//
// ©2016 Microchip Technology Inc. and its subsidiaries. You may use this
// software and any derivatives exclusively with Microchip products.
//
// THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
// EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
// WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A PARTICULAR
// PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION WITH ANY
// OTHER PRODUCTS, OR USE IN ANY APPLICATION.
//
// IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
// INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
// WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
// BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
// FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
// ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
// THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
//
// MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE TERMS.
//******************************************************************************
//
// Memory Map
//   -----------------
//   |    0x0000     |   Reset vector
//   |    0x0008     |   High Priority Interrupt vector
//   |    0x0018     |   Low Priority Interrupt vector
//   |               |
//   |  Boot Block   |   (this program)
//   |               |
//   |    0x0F00     |   Re-mapped Reset Vector (Actual address
//   |    0x0F08     |   Re-mapped High Priority Interrupt Vector
//   |    0x0F18     |   Re-mapped Low Priority Interrupt Vector
//   |       |       |
//   |               |
//   |     0x1000    |   User program space
//   |               |
//   |       |       |
//   |               |
//   | End of Flash  |
//   -----------------
//
//
//
//
// *****************************************************************************

#define  READ_VERSION   0
#define  READ_FLASH     1
#define  WRITE_FLASH    2
#define  ERASE_FLASH    3
#define  READ_EE_DATA   4
#define  WRITE_EE_DATA  5
#define  READ_CONFIG    6
#define  WRITE_CONFIG   7
#define  CALC_CHECKSUM  8
#define  RESET_DEVICE   9
#define  GET_REGISTRY   10
#define  START_DL   	11



// *****************************************************************************
    #include <xc.h>       // Standard include
    #include <stdint.h>
    #include <stdbool.h>
    #include <string.h>
    #include "bootload.h"
    #include "mcc.h"
    #include "timers.h"

// *****************************************************************************
void     Get_Buffer (void);     // generic comms layer
uint8_t  Get_Version_Data(void);
uint8_t  Read_Flash(void);
uint8_t  Write_Flash(void);
uint8_t  Erase_Flash(void);
uint8_t  Read_EE_Data(void);
uint8_t  Write_EE_Data(void);
uint8_t  Read_Config(void);
uint8_t  Write_Config(void);
uint8_t  Calc_Checksum(void);
void     StartWrite(void);
void     BOOTLOADER_Initialize(void);
bool     ValidateFlashChecksum (void);
void 	 Check_Device_Reset (void);

// *****************************************************************************
#define	MINOR_VERSION	0x00       // Version
#define	MAJOR_VERSION	0x01

// To be device independent, these are set by mcc in memory.h
#define  LAST_WORD_MASK              (WRITE_FLASH_BLOCKSIZE - 1)
#define  NEW_RESET_VECTOR            0x0F00
#define  START_OF_APP	             0x0F00
#define  NEW_INTERRUPT_VECTOR_HIGH   0x0F08
#define  NEW_INTERRUPT_VECTOR_LOW    0x0F18

#define _str(x)  #x
#define str(x)  _str(x)
// *****************************************************************************


// *****************************************************************************
    uint16_t check_sum;      // Checksum accumulator
    uint16_t wait_timer;      // 
    uint16_t counter;        // General counter
    uint8_t rx_data;
    uint8_t tx_data;
    bool reset_pending  = false;
// Force variables into Unbanked for 1-cycle accessibility 
    uint8_t EE_Key_1;
    uint8_t EE_Key_2;

	volatile frame_t  frame;

// bh - patch for version troubles
#define VERSION_LEN		16
char FlashVersion [VERSION_LEN] = 
{
   0x41,1,0,1,0,0,4,0x54,0,0,0x40,0x40,0,0x23,0x1F,0x3C
};	

extern uint8_t CheckTimer (void);

// *****************************************************************************
// The bootloader code does not use any interrupts.
// However, the application code may use interrupts.
// The interrupt vector on a PIC18F is located at
// address 0x0008 (High) and 0x0018 (Low). 
// The following function will be located
// at the interrupt vector and will contain a jump to
// the new application interrupt vectors
// *****************************************************************************
/***
    asm ("PSECT intcode,global,reloc=2,class=CODE,delta=1");
    asm ("GOTO NEW_INTERRUPT_VECTOR_HIGH");

    asm ("PSECT intcodelo,global,reloc=2,class=CODE,delta=1");
    asm ("GOTO NEW_INTERRUPT_VECTOR_LOW");
****/
    asm ("PSECT intcode,global,reloc=2,class=CODE,delta=1");
    asm ("GOTO 0x0F08");

    asm ("PSECT intcodelo,global,reloc=2,class=CODE,delta=1");
    asm ("GOTO 0x0F18");

/***********************************************************
    Subroutine:         GoToBootblock()

    Description:
		This routine determines if the application left a message in EEprom
		asking the bootloader to remain inside the bootblock.  
                    
    Inputs: 
		This routine must:
		 1. Read a msg to the bootblock:  0x55 from TMR3L
            
    Outputs:
    	true -	stay in bootblock
    	false -  exit the bootblock 
            
*************************************************************/
uint8_t GoToBootblock ()
{
	//return ((TMR3L == BB_CMD_LO) && (TMR3H == BB_CMD_HI));
	return (TMR3L == BB_CMD_LO);
}


/***********************************************************
    Subroutine:   EraseBootblockMessage()

    Description:
		The work of the bootblock has been completed.
		This routine must erase the message for it that had been left in EEprom by the application  
                    
    Inputs: 
		This routine must:
		 1. Read a msg to the bootblock:  0x55 from TMR3L
            
    Outputs:
		 
            
*************************************************************/
void EraseBootblockMessage ()
{
	TMR3L = TMR3H = 0;
}

//*****************************************************************************
void BOOTLOADER_Initialize ()
{
	PIE1 = 0;
	PIE2 = 0;
	PIE3 = 0;
	PIE4 = 0;
	PIE5 = 0;

	PIR1 = 0;
	PIR2 = 0;
	PIR3 = 0;
	PIR4 = 0;
	PIR5 = 0;

    PIR1bits.TX1IF = 1;
    PIR3bits.TX2IF = 1;

	// zero out key variables
	wait_timer = 0;
	// Start up Timer0 to keep track of time
	TimersInitialize ();
	// Validate the Flash Checksum
    if (ValidateFlashChecksum () == false)
    {
		// Checksum has failed:  no valid application is installed
        Run_Bootloader (0, 0);     // generic comms layer
    }
	// Check to see if a message has been left from the application
	else if (GoToBootblock())
    {
		// bh - temporary test code
		EraseBootblockMessage ();

		// A Message has been received from the application:  Stay in the bootblock!
        Run_Bootloader (1, 0);     

    }
	// No valid code present and no msg in the mailbox.  Probably, we should wait 10 seconds though
	else if ((TMR3L != BB_IGNORE_LO) || (TMR3H != BB_IGNORE_HI))
    {
		// Wait 10 seconds in case a message is received
        Run_Bootloader (0, 1);     
    }
	BAUDCON2 = 0;
	// We must erase the message from the application before exiting:
	EraseBootblockMessage ();
	// The bootblock's work has been completed.  We must now jump to the application
    STKPTR = 0x00;
    asm ("goto  "  str(NEW_RESET_VECTOR));
}

// **************************************************************************************
// Read_EE_Checksum
//
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x04> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x04> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <Data>..<data>]
//
// *****************************************************************************
uint16_t Read_EE_Checksum()
{
	char cs [6];
	uint16_t checksum;

	EECON1 = 0;
    EEADR  = 0xFE;
    EEADRH = 0x03;
    for (uint8_t  i = 0; i < 2; i++)
    {
        EECON1bits.RD = 1;
        cs[i] = EEDATA;
        ++EEADR;
    }
	// get msb
	checksum = cs[1];
	checksum <<= 8;
	// get lsb
	checksum += cs[0];
    return (checksum);
}
// *****************************************************************************
bool ValidateFlashChecksum ()
{
// ******************************************************************
//  Check an IO pin to force entry into bootloader
// ******************************************************************

//#info  "You may need to additional delay here between enabling weak pullups and testing the pin."
//    for (uint8_t i = 0; i != 0xFF; i++) NOP();

    //if (IO_PIN_ENTRY_PORT_PIN == IO_PIN_ENTRY_RUN_BL)
    //if (1)
    //{
    //    return (true);
    //}

// **************************************************************************************
//  Calculate a checksum over the application area and compare to pre-calculated checksum
// **************************************************************************************

    uint16_t  Stored_Checksum;

    frame.address_L = (uint8_t)  (START_OF_APP & 0x00FF);	// bh - was NEW_RESET_VECTOR
    frame.address_H = (uint8_t) ((START_OF_APP & 0xFF00) >> 8);
    frame.address_U = (uint8_t) 0;
    frame.data_length = (uint16_t) (END_FLASH - START_OF_APP + 1);
    Calc_Checksum ();
    Stored_Checksum = Read_EE_Checksum();

    if (Stored_Checksum != check_sum)
    {
        return (false);
    }
	return (true);
}

// *****************************************************************************
// Unlock and start the write or erase sequence.
// *****************************************************************************
void Start_EE_Write()
{
	EECON1 = 0;
	PIR2bits.EEIF = 0;			// this flag gets set by the hardware when the Write Operation has been completed
    EECON1bits.WREN = 1;		// Activate Write Enable
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;       // Start the write
    NOP();
    NOP();
    EECON1bits.WREN = 0;		// Turn off Write Enable
}

// *****************************************************************************
// Save_Checksum -- in addresses 0x3FE and 0x3FF
// bh
//
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x05> <0x00><0x00><0x55><0xAA> <0x00><0x00><0x00><0x00> <Data>..<data>]
// OUT:  [<0x05> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <0x01>]
// *****************************************************************************
void  Save_Checksum()
{
    EEADR  = 0xFD;
    EEADRH = 0x03;
    for (uint8_t i = 0; i < 2; i++)
    {
        ++EEADR;
        EEDATA = frame.data[i];
        Start_EE_Write ();
        while (PIR2bits.EEIF == 0);  // wait until previous write complete
    }
}


// *****************************************************************************
// Commands
//
//        Cmd     Length----------------   Address---------------
// In:   [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <VERL><VERH>]
// *****************************************************************************
uint8_t GetNetworkRegistry()
{
	// start out with frame data of all zeroes
	memset (frame.data, 0x00, WRITE_FLASH_BLOCKSIZE);
	frame.data[0] = 'M';
	// Set the return length
	frame.data_length = 9;
    return  (9+9);   // total length to send back 9 byte header + 9 byte payload
}

// *****************************************************************************
// Commands
//
//        Cmd     Length----------------   Address---------------
// In:   [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <VERL><VERH>]
// *****************************************************************************
uint8_t InitiateFirmwareDownload()
{
	char Network_Id = frame.data[0];
	char Device_Id = frame.data[1];

    frame.data[0] = COMMAND_SUCCESS;
	if ((Network_Id != 0)
	 || (Device_Id != 'M'))
	{
		// The bootblock can only process Main Board downloads
		frame.data[0] = ERROR_INVALID_COMMAND;
	}

	// Set the return length
	frame.data_length = 1;
    return  (9+1);   // total length to send back 9 byte header + ACK
}
//----------------------------------------
void CalculateMsgChecksum (char len)
{
	unsigned char i,checksum;

	frame.checksum = 0;
	checksum = 0;
	for (i = 0;  i < len;  i++)
	{	
		checksum += frame.buffer[i];
	}
	// Save the calculated checksum
	frame.checksum = checksum;
}

// *****************************************************************************
uint8_t  ProcessBootBuffer()
{
    uint8_t   len;
    EE_Key_1 = frame.EE_key_1;
    EE_Key_2 = frame.EE_key_2;

	// Test the command field and sub-command.
    switch (frame.command)
    {
    case    READ_VERSION:
        len = Get_Version_Data();
        break;
    case    READ_FLASH:
        len = Read_Flash();
        break;
    case    WRITE_FLASH:
        len = Write_Flash();
        break;
    case    ERASE_FLASH:
        len = Erase_Flash();
        break;
    case    READ_EE_DATA:
        len = Read_EE_Data();
        break;
    case    WRITE_EE_DATA:
        len = Write_EE_Data();
        break;
    case    READ_CONFIG:
        len = Read_Config();
        break;
    case    WRITE_CONFIG:
        len = Write_Config();
        break;
    case    CALC_CHECKSUM:
        len = Calc_Checksum();
		// bh ---- only save the Checksum in nonvolatile memory if the Unlock Sequence (0x55-0xAA) is correct
		if ((EE_Key_1 == EE_KEY_VAL1) && (EE_Key_2 == EE_KEY_VAL2))
		{
			Save_Checksum ();
		}
        break;
    case    RESET_DEVICE:
        reset_pending = true;
		// bh --- Make sure to clear the message -- and skip the 10-second wait after reset
		TMR3L = BB_IGNORE_LO;
		TMR3H = BB_IGNORE_HI;
        len = 10;
    	frame.data[0] = COMMAND_SUCCESS;
		frame.data_length = 1;
        break;
    case    GET_REGISTRY:
        len = GetNetworkRegistry();
        break;
    case    START_DL:
        len = InitiateFirmwareDownload();
        break;

    default:
        frame.data[0] = ERROR_INVALID_COMMAND;
        len = 10;
    }
	// Calculate the checksum on the return message
	CalculateMsgChecksum(len);
    return (len);
}

// *****************************************************************************
// Commands
//
//        Cmd     Length----------------   Address---------------
// In:   [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x00> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <VERL><VERH>]
// *****************************************************************************
uint8_t  Get_Version_Data()
{
	uint8_t  i;

// bh - patch
	for ( i = 0;  i < VERSION_LEN; i++ )
	{
		frame.data[i] = FlashVersion[i];
	}
	// Set the return length
	frame.data_length = 16;
/****
    frame.data[0] = MINOR_VERSION;
    frame.data[1] = MAJOR_VERSION;
    frame.data[2] = 0;       	// Max packet size (256)
    frame.data[3] = 1;
    frame.data[4] = 0;
    frame.data[5] = 0;
    EECON1 = 0x40;       // bh - was 0xC0
    TBLPTRL = 0xFE;
    TBLPTRH = 0xFF;               // Get device ID
    TBLPTRU = 0x3F;
    EECON1 = 0xC0;       // bh - was 0xC0
    asm("TBLRD *+");  	
    frame.data[6] = TABLAT;
    asm("TBLRD *+");   		
    frame.data[7] = TABLAT;
    frame.data[8] = 0;
    frame.data[9] = 0;

    frame.data[10] = ERASE_FLASH_BLOCKSIZE;
    frame.data[11] = WRITE_FLASH_BLOCKSIZE;

    EECON1 = 0x40;       // bh - was 0xC0
    TBLPTRL = 0x00;
    TBLPTRH = 0x00;
    TBLPTRU = 0x30;
    EECON1 = 0xC0;       
    for (i = 12; i < 16; i++)
    {
        asm("TBLRD *+");
        frame.data[i] = TABLAT;
    }
*****/

    return  25;   // total length to send back 9 byte header + 16 byte payload
}

// *****************************************************************************
// Read Flash
//        Cmd     Length----------------   Address---------------  Data ---------
// In:   [<0x01> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x01> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <Data>..<data>]
// *****************************************************************************
uint8_t Read_Flash()
{
    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
    EECON1 = 0x80;
    for (uint16_t i = 0; i < frame.data_length; i ++)
    {
        asm("TBLRD *+");
        frame.data[i]  = TABLAT;
    }

    return (frame.data_length + 9);
}


// *****************************************************************************
// Write Flash
//        Cmd     Length----- Keys------   Address---------------  Data ---------
// In:   [<0x02> <0x00><0x00><0x55><0xAA> <0x00><0x00><0x00><0x00> <Data>..<data>]
// OUT:  [<0x02> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <0x01>]
// *****************************************************************************
uint8_t Write_Flash()
{
    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
	EECON1 = 0xA4;       // Setup writes
    for (uint16_t  i = 0; i < frame.data_length; i ++)
    {
        TABLAT = frame.data[i];
        if (TBLPTR >= END_FLASH)
        {
            frame.data[0] = ERROR_ADDRESS_OUT_OF_RANGE;
            return (10);
        }
        asm("TBLWT *+");
        if (((TBLPTRL & LAST_WORD_MASK) == LAST_WORD_MASK)
          || (i == frame.data_length - 1))
        {
            asm("TBLRD *-");
            StartWrite();
            asm("TBLRD *+");
        }
    }
	// Set the return length
	frame.data_length = 1;
    frame.data[0] = COMMAND_SUCCESS;
    EE_Key_1 = 0x00;  // erase EE Keys
    EE_Key_2 = 0x00;
    return (10);
}

// *****************************************************************************
// Erase Program Memory
// Erases data_length rows from program memory
//        Cmd     Length----- Keys------   Address---------------  Data ---------
// In:   [<0x03> <0x00><0x00><0x55><0xAA> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x03> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <0x01>]
// *****************************************************************************
uint8_t Erase_Flash ()
{
    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
    for (uint16_t i=0; i < frame.data_length; i++)
    {
        if (TBLPTR >= END_FLASH)
        {
            frame.data[0] = ERROR_ADDRESS_OUT_OF_RANGE;
            return (10);
        }
        EECON1 = 0x94;       // Setup writes
        StartWrite();
        TBLPTR += ERASE_FLASH_BLOCKSIZE;
    }
	// Set the return length
	frame.data_length = 1;
    frame.data[0]  = COMMAND_SUCCESS;
    frame.EE_key_1 = 0x00;  // erase EE Keys
    frame.EE_key_2 = 0x00;
    return (10);
}

// **************************************************************************************
// Read_EE_Data
//
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x04> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x04> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <Data>..<data>]
//
// *****************************************************************************
uint8_t Read_EE_Data()
{
    EEADR  = frame.address_L;
    EEADRH = frame.address_H;
	EECON1 = 0;
    for (uint8_t  i = 0; i < frame.data_length; i++)
    {
        EECON1bits.RD = 1;
        frame.data[i] = EEDATA;
        if (++EEADR == 0x00)
        {
            ++ EEADRH;
        }
    }
    return (frame.data_length+9);
}
// *****************************************************************************
// Write_EE_Data
//
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x05> <0x00><0x00><0x55><0xAA> <0x00><0x00><0x00><0x00> <Data>..<data>]
// OUT:  [<0x05> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <0x01>]
// *****************************************************************************
uint8_t  Write_EE_Data()
{
    EEADR  = frame.address_L;
    EEADRH = frame.address_H;
	EECON1 = 0x04;  //   b'00000100';     // Setup for EEData
    for (uint8_t i = 0; i < frame.data_length; i++)
    {
        while (EECON1bits.WR == 1);  // wait until previous write complete
        if (++EEADR == 0x00)
        {
            ++EEADRH;
        }
        EEDATA = frame.data[i];
        StartWrite ();
    }
	// Set the return length
	frame.data_length = 1;
    frame.data[0] = COMMAND_SUCCESS;
    return 10;
}

// *****************************************************************************
// Read Config Words
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x06> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x06> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <Data>..<data>]
// *****************************************************************************
uint8_t Read_Config ()
{
    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
	EECON1   = 0xC0;
    for (uint8_t  i = 0; i < frame.data_length; i++)
    {
        asm("TBLRD *+");
        frame.data[i] = TABLAT;
    }
    return (9 + frame.data_length);           // 9 byte header + 4 bytes config words
}

// *****************************************************************************
// Write Config Words
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x07> <0x00><0x00><0x55><0xAA> <0x00><0x00><0x00><0x00> <Data>..<data>]
// OUT:  [<0x07> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// *****************************************************************************
uint8_t Write_Config ()
{
    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
    if (TBLPTR > 0x20000F)    
        EECON1 = 0xC4;       // Setup writes
    else
        EECON1 = 0x84;
    for (uint8_t  i= 0; i < frame.data_length; i ++)
    {
        TABLAT = frame.data[i];
        asm("TBLWT *");
        StartWrite();
        ++ TBLPTRL;
    }
	// Set the return length
	frame.data_length = 1;
    frame.data[0] = COMMAND_SUCCESS;
    EE_Key_1 = 0x00;  // erase EE Keys
    EE_Key_2 = 0x00;
    return (10);
}

// *****************************************************************************************
// Calculate Checksum
// In:	[<0x08><DataLengthL><DataLengthH> <unused><unused> <ADDRL><ADDRH><ADDRU><unused>...]
// OUT:	[9 byte header + ChecksumL + ChecksumH]
//        Cmd     Length-----              Address---------------  Data ---------
// In:   [<0x08> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00>]
// OUT:  [<0x08> <0x00><0x00><0x00><0x00> <0x00><0x00><0x00><0x00> <CheckSumL><CheckSumH>]
// *****************************************************************************
uint8_t Calc_Checksum()
{
    uint16_t  i;
    uint16_t  length;

    TBLPTRL = frame.address_L;
    TBLPTRH = frame.address_H;
    TBLPTRU = frame.address_U;
    EECON1 = 0x80;
    check_sum = 0;
    length = frame.data_length;
    for (i = 0; i < length; i += 2)
    {
        asm("TBLRD *+");
        check_sum += (uint16_t)TABLAT;
        asm("TBLRD *+");
        check_sum += (uint16_t)TABLAT << 8;
     }
     frame.data[0] = (uint8_t) (check_sum & 0x00FF);
     frame.data[1] = (uint8_t)((check_sum & 0xFF00) >> 8);
	// Set the return length
	frame.data_length = 2;
     return (11);
}




// *****************************************************************************
// Unlock and start the write or erase sequence.
// *****************************************************************************
void StartWrite(void)
{
    EECON2 = EE_Key_1;
    EECON2 = EE_Key_2;
    EECON1bits.WR = 1;       // Start the write
    NOP();
    NOP();
    return;
}

// *****************************************************************************
// Check to see if a device reset had been requested.  We can't just reset when
// the reset command is issued.  Instead we have to wait until the acknowledgement
// is finished sending back to the host.  Then we reset the device.
// *****************************************************************************
void Check_Device_Reset (void)
{
    if (reset_pending == true)
    {
		wait_timer = 0;
		while (1)
		{
			// Check to see if 10ms has passed
			CheckTimer ();
			if (wait_timer >= 2)
			{
				// Time has expired.  We can exit the bootblock.
		        RESET();
			}
		}
    }
    return;
}

// *****************************************************************************
// *****************************************************************************
