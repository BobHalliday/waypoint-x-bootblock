/***********************************************************
	Module Name:	timers.c

	Author:			Bob Halliday, September, 2018
					bohalliday@gmail.com    (781) 863-8245

	Description:
	Software Timer creation and maintenance support is provided
	by this module.

	Hardware Timers utilizied in Model 1000 Firmware:
	
	NAME	FUNCTION
	Timer0	Time Base for 5ms timer.  
			Numerous Software Timers will run off this Time Base.
	Timer1	Timers with a resolution of less than 5ms will be implemented on Timer1
	
	Subroutines:	TimersInitialization()
					Timers5MsInterrupt ()
                    TimersRestartTimer ()
                    TimersTimerIsExpired ()

	Revision History:


*************************************************************/
#define BODY_TIMERS

    #include <xc.h>       // Standard include
    #include <stdint.h>
    #include <stdbool.h>
#include "timers.h"  



/********************************************
The following routines are for Timer0 support: 
*********************************************/
/***********************************************************
	Subroutine:	TimersInitialize()

	Description:
    	Initialize Timer0 to interrupt the system once every '5' milliseconds.
		Clock source = FOSC/4 = 12MHz/4 = 3.0MHz = 0.33us
		Select Prescaler = 256.  So 256 * 0.33 = 84.48us
		To get 5ms:  5000/84.48	= 59.185 = 59

		Enable Timer1 to run the pwm output

	Inputs:

	Outputs:																		  
		TOCON
		TMR0
		
		
	Locals:
*************************************************************/
void TimersInitialize (void)
{
	// Timer0 provides the time base to the system.  Start Time Base at 5 ms.
	// prescaler=256, running at 5.0MHz, we need 98 ticks to get 5ms
	// Enable Timer 0
  	T0CON = TIMERS_0_CONFIGURE;                 
	// 98 ticks is 255 - 98 = 157 
	TMR0H = 0xFF;
	TMR0L = (char) TIMERS_5_MS;	

}

/*******************************************************************************
	Function:	TimersRestartTimer()

	Description:
	This subroutine will reset a selected 5ms Timer to zero.

	Inputs:
		TimerIndex - Index into TimersArray [ ]

	Outputs:
		TimersArray [TimerIndex] = 0

	Locals:
********************************************************************************/



/* ======== END OF FILE ======== */
