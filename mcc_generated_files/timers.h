/***************************************************************************
	File	:	timers.h
	Title	:	Timer Management Header File
	Author	:	Bob Halliday
					bohalliday@gmail.com    (781) 863-8245
	Created	:	September, 2018
	Copyright:	(C) 2018 Civiq SmartSystems

	Description:
		Timers Definitions for bootblock timers.c source file

	Contents:

	Revision History:


***************************************************************************/
#ifndef HEADER_TIMERS
#define HEADER_TIMERS

/************************************************
 define SCOPE to allow RAM variables to be defined directly or as externs
*************************************************/
#ifdef SCOPE
#undef SCOPE
#endif

#ifdef BODY_TIMERS
#define SCOPE
#else
#define SCOPE	extern
#endif

/*******************************
  Section 1:  Equate Definitions:
*********************************/
// The Time Base is defined as 5ms 
#define TIME_BASE 					5                               // 5 ms
#define ONE_TENTH_OF_A_SECOND       (int)(100/TIME_BASE)
#define TIMERS_POWER_ACTIVE         (int)(250/TIME_BASE)

#define ONE_FULL_SECOND             (int)(1000/TIME_BASE)
#define SLAVE_CONNECT_TIME          (int)(1000/TIME_BASE)			// should be 1000
#define MILLISECONDS_5              1
#define SECONDS_1                   (int)(MILLISECONDS_5*200)
#define TIMERS_WAIT_PERIOD			1400							// = 7 seconds

// Timers Clock = 20Mhz / 4 = 0.20 microseconds
// Prescaler=256, clock source = FOSC/4,  0.20usec * 256 = 98 usec per timer tick
#define TIMERS_0_CONFIGURE          0xC7
#define TIMERS_5_MS                 (0x00 - 98)                        





/*******************************
  Section 2:  RAM Variables:
*********************************/

// Private Variables:
#ifdef BODY_TIMERS




#endif


// Public Variables:
#ifdef BODY_TIMERS
//#pragma udata timers_data=0x070
#endif




/*******************************
  Section 3:  Function prototypes
*********************************/
void  TimersInitialize (void);
void  Timers5MsInterrupt (void);



/*******************************
  Section 4:  Macros
*********************************/



/********************************
  Section 5:  ROM Tables
*********************************/

#ifdef BODY_TIMERS


#endif



#undef SCOPE
#endif

/* ======== END OF FILE ======== */
