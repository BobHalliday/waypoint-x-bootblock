/**
  EUSART2 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    eusart2.c

  @Summary
    This is the generated driver implementation file for the EUSART2 driver using LIN Library

  @Description
    This source file provides APIs for EUSART2.
    Generation Information :
        Product Revision  :  LIN Library - 2.2
        Device            :  PIC18F46K22
        Driver Version    :  2.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.45
        MPLAB 	          :  MPLAB X 4.15
*/


/**
  Section: Included Files
*/
#include "eusart2.h"

volatile eusart2_status_t eusart2RxLastError;
#define ISR_UART_ERRORS			 0x06

/**
  Section: EUSART2 APIs
*/
void (*EUSART2_FramingErrorHandler)(void);
void (*EUSART2_OverrunErrorHandler)(void);
void (*EUSART2_ErrorHandler)(void);

void EUSART2_DefaultFramingErrorHandler(void);
void EUSART2_DefaultOverrunErrorHandler(void);
void EUSART2_DefaultErrorHandler(void);

void EUSART2_Initialize(void)
{
    // Set the EUSART2 module to the options selected in the user interface.

    // ABDOVF no_overflow; CKTXP async_noninverted_sync_fallingedge; BRG16 16bit_generator; WUE disabled; ABDEN disabled; DTRXP not_inverted; 
    BAUDCON2 = 0x08;

    // SPEN enabled; RX9 8-bit; CREN enabled; ADDEN disabled; SREN disabled; 
    RCSTA2 = 0x90;

    // TX9 8-bit; TX9D 0; SENDB sync_break_complete; TXEN enabled; SYNC asynchronous; BRGH hi_speed; CSRC slave_mode; 
    TXSTA2 = 0x24;

    // 
    SPBRG2 = 0x56;

    // 
    SPBRGH2 = 0x00;

    eusart2RxLastError.status = 0;

}

bool EUSART2_is_rx_ready(void)
{
    return PIR3bits.RC2IF;
}

#ifdef DEAD_CODE
bool EUSART2_is_tx_ready(void)
{
    return (bool)(PIR3bits.TX2IF && TXSTA2bits.TXEN);
}

bool EUSART2_is_tx_done(void)
{
    return TXSTA2bits.TRMT;
}

eusart2_status_t EUSART2_get_last_status(void){
    return eusart2RxLastError;
}

#endif

uint8_t EUSART2_Read(void)
{
    while(!PIR3bits.RC2IF)
    {
        CLRWDT();
    }

	// checking for errors, and clearing them if they occurred.
	if (RCSTA2 & ISR_UART_ERRORS)
		{
		RCSTA2bits.CREN=0;
		RCSTA2bits.CREN=1;
		}

    return RCREG2;
}

void EUSART2_Write(uint8_t txData)
{
    while(0 == PIR3bits.TX2IF)
    {
        CLRWDT();
    }

    TXREG2 = txData;    // Write the data byte to the USART.
}



#ifdef DEAD_CODE

void EUSART2_DefaultFramingErrorHandler(void){}

void EUSART2_DefaultOverrunErrorHandler(void){
    // EUSART2 error - restart

    RCSTA2bits.CREN = 0;
    RCSTA2bits.CREN = 1;

}

void EUSART2_DefaultErrorHandler(void){
}

void EUSART2_SetFramingErrorHandler(void (* interruptHandler)(void)){
    EUSART2_FramingErrorHandler = interruptHandler;
}

void EUSART2_SetOverrunErrorHandler(void (* interruptHandler)(void)){
    EUSART2_OverrunErrorHandler = interruptHandler;
}

void EUSART2_SetErrorHandler(void (* interruptHandler)(void)){
    EUSART2_ErrorHandler = interruptHandler;
}

#endif

/**
  End of File
*/
